/**
 * Files need 
 *
 * 'A/jquery-ui/ui/minified/jquery.iframe-transport.min.js',
 * 'A/jquery-ui/ui/minified/jquery.fileupload.min.js',
 * 'Application/directives/ng-upload.js'
 *
 *
 *  SUPPORT BOTH SINGLE AND MUTIPLE FILE ULOAD
 *  For the case of single file upload return will Object
 *  In the case of multiple you need to specify multiple attribute in the file input , return type in this case is array of File Model Objects
 */
angular.module('ngUpload', []).directive('ngUpload',function($parse,storageService){
  
/*  
    It returns the URL for Logo Image Files using the path recieved from file object.
*/
  var getLogoUrl = function(filePath){
    return "/logoDown?path="+ filePath +"&isLogo=1&fileType=jpg";
  }

  var getImageUploadUrl = function(name) {
                  var UPLOAD_CONFIG = {
                     'FILE_STORE_URL': '/sourceasy/up.php'
                  };
                  if(name == 'FILE_STORE_URL'){
                    var regExp = /^(www|stage|test|dev|pp)(.*)/i;
                    var matched = window.location.host.match(regExp);
                    if(matched){
                      var fileUrlPrefix = '';
                      if(matched[1] == "www"){fileUrlPrefix = "file";}
                      else {fileUrlPrefix = matched[1]+"file";}
                      if(fileUrlPrefix == 'devfile'){
                        matched[2] += ':81'; // port defined according to environment type
                      }else if(fileUrlPrefix == 'stagefile'){
                        matched[2] += ':10099'; // port defined according to environment type
                      }
                      var fileStoreUrl = fileUrlPrefix + matched[2];
                      return (window.location.protocol + '//' + fileStoreUrl+UPLOAD_CONFIG[name]);
                    }
                 }
                 return UPLOAD_CONFIG[name]; 
  }
  return {
    restrict: 'EA',
    link:function(scope, element, attrs){
        var multipleUpload = (angular.isDefined(attrs.multiple)) ? true : false;
        var loaderclass = (angular.isDefined(attrs.loaderclass)) ? attrs.loaderclass : false;
        var isLogo = (angular.isDefined(attrs.islogo)) ? 1 : 0; 
        var regexCheck;
             
        var upUrl = getImageUploadUrl('FILE_STORE_URL');
        $('#'+attrs.id+"Button").on('click', function(){
                    $('#'+attrs.id).click();   
        });
        $(function () { 
          $('#'+attrs.id).fileupload({
            url: upUrl,  
            dataType: 'json',
            add : function(e,data){
              var goUpload = true;
              var uploadFile = data.files[0];
              if(angular.isDefined(attrs.filetype)){
                if(attrs.filetype == 'excel'){
                  regexCheck = /^[a-zA-Z0-9_'\-()\s]*[\.]{1,1}(xls|xlsx|csv|ppt|pptx)$/;
                }else{
                  regexCheck = /^[a-zA-Z0-9_'\-()\s]*[\.]{1,1}(bmp|gif|jpg|jpeg|png)$/;
                }
              }else{
                regexCheck = /^[a-zA-Z0-9_'\-()\s]*[\.]{1,1}(bmp|gif|jpg|jpeg|png|xls|xlsx|csv|doc|docx|pdf|txt|ppt|pptx)$/;
              } 
              if (!(regexCheck.test(uploadFile.name))) {
                alert("Please select a valid file.(filename can only contain(a-z0-9'()_- )");
                goUpload = false;
              }
              if (goUpload == true) {
                data.submit();
              }     
            },
            start: function(e,t){
              if(loaderclass){$('.'+loaderclass).show();
            };
            },
            done: function (e,data) {
                if(loaderclass){$('.'+loaderclass).hide();}  
                scope.$apply(function () {                  
                   if(multipleUpload){
                    var model = $parse(attrs.ngUpload);
                    if(typeof model(scope) == "undefined") model.assign(scope,[]); 
                    model(scope).push(data.result.result);
                  } else {  
                    if(isLogo == 1) {
                      scope[attrs.ngUpload] = getLogoUrl(data.result.result.filePath);
                    }else{                     
                      scope[attrs.ngUpload] = data.result.result;
                    }
                  }
                });
            }
          });
      });
    }
  }
});